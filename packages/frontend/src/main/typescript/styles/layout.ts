import { FelaStyleObject } from "../utils/fela/types"

export const container: FelaStyleObject = {
	paddingLeft: "12em",
	paddingRight: "12em",
}
