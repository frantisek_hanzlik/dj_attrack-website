export interface ContactMeanType {
	name: string
	key: string
}

export interface ContactMean {
	type: ContactMeanType
	url: string
	linkText?: string
	key: string
}
