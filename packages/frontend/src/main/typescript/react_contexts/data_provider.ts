import { ServiceModel } from "../models/service"
import React from "react"
import { ValueNotFoundError } from "../errors/generic"
import { Person } from "../models/contacts/person"
import { ImageModel } from "../models/image"

interface DataProvider {
	getServices: () => ServiceModel[]
	getServiceById: (id: string) => ServiceModel
	getContacts: () => Person[]
	getGalleryImages: () => ImageModel[]
}

export const DataProviderContext = React.createContext<DataProvider>({
	getServices: () => [],
	getServiceById: () =>
		(() => {
			throw new ValueNotFoundError("This context contains the default value which cannot provide any results")
		})(),
	getContacts: () => [],
	getGalleryImages: () => [],
})
