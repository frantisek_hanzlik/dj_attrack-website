import { CssProperty } from "./css_property"
import { FelaStyleObject } from "../../utils/fela/types"

export enum BoundingBoxType {
	MARGIN,
	PADDING,
}

export class BoundingBox implements CssProperty {
	public readonly left?: string
	public readonly right?: string
	public readonly bottom?: string
	public readonly top?: string

	public readonly type: BoundingBoxType = BoundingBoxType.PADDING

	public static newAll(all: string) {
		return Object.assign(new this(), {
			left: all,
			right: all,
			bottom: all,
			top: all,
		})
	}

	public static newXY(initializer: { x?: string; y?: string }) {
		return Object.assign(new this(), {
			left: initializer.x,
			right: initializer.x,
			bottom: initializer.y,
			top: initializer.y,
		})
	}

	public toFelaStyle(): FelaStyleObject {
		switch (this.type) {
			case BoundingBoxType.PADDING:
				return {
					paddingLeft: this.left,
					paddingRight: this.right,
					paddingBottom: this.bottom,
					paddingTop: this.top,
				}
			case BoundingBoxType.MARGIN:
				return {
					marginLeft: this.left,
					marginRight: this.right,
					marginBottom: this.bottom,
					marginTop: this.top,
				}
		}
	}
}
