import { Theme } from "../../../theme"
import { BoundingBox } from "../../../properties/bounding_box"

let themeDummy: Theme

export const layout: (getTheme: () => Theme) => typeof themeDummy.components.layout = getTheme => ({
	root: {
		background: {
			color: "hsl(0, 0%, 12.5%)",
			image: "linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url(/static/images/patterns/chalkboard.jpg)",
		},
		text: {
			color: "white",
		},
		content: {
			width: "80%",
			get shadow() {
				return getTheme().common.shadows.dark.normal
			},
			bounds: [BoundingBox.newXY({ y: "3rem" })],
		},
	},
	composition: {
		navbar: {
			height: "3.5rem",
		},
	},
})
