import { BoundingBox } from "../../../properties/bounding_box"
import { ThemeStyles as ServiceListThemeStyles } from "../../../../components/reusable/service_list/styles"
import { Theme } from "../../../theme"

export const serviceList: (getTheme: () => Theme) => ServiceListThemeStyles = () => ({
	root: {
		bounds: [BoundingBox.newXY({ y: "6rem" })],
		background: {
			color: "hsl(0, 0%, 5%)",
		},
	},
	subcomponents: {
		serviceButton: {},
	},
})
