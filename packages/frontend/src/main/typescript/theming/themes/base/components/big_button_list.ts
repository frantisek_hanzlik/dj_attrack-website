import { Theme } from "../../../theme"

let themeDummy: Theme

export const bigButtonBar: (getTheme: () => Theme) => { subcomponents: { bigButton: typeof themeDummy.components.bigButton } } = () => ({
	subcomponents: {
		bigButton: {
			width: "calc(50px + 10vw)",
			border: {
				width: "calc(10px + 1vw)",
			},
			icon: {
				width: "50%",
				height: "50%",
				margin: {
					bottom: "20px",
				},
			},
			hoverAnimation: {
				duration: "0.25s",
				easingFunction: "linear",
				border: {
					width: "calc(5px + 1vw)",
				},
			},
		},
	},
})
