import React from "react"
import dynamic from "next/dynamic"
import { FelaComponent } from "react-fela"
import { Layout } from "../layout/layout"

const DynamicCalendar = dynamic(async () => (await import("../reusable/calendar")).Calendar, {
	ssr: false,
})

export class Calendar extends React.Component {
	public render() {
		return (
			<Layout>
				<FelaComponent
					style={{
						display: "flex",
						width: "100%",
						alignItems: "center",
						justifyContent: "center",
					}}
				>
					<DynamicCalendar />
				</FelaComponent>
			</Layout>
		)
	}
}
