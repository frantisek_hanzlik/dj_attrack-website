import React from "react"
import { FelaComponent, StyleProps } from "react-fela"
import { Layout } from "../layout/layout"
import { DataProviderContext } from "../../react_contexts/data_provider"
import { ImageModel } from "../../models/image"
import { Theme } from "../../theming/theme"
import { Image } from "../reusable/image"

export class Gallery extends React.Component {
	public render() {
		return (
			<Layout>
				<FelaComponent
					style={(props: StyleProps<Theme>) => ({
						display: "flex",
						width: "100%",
						position: "relative",
						minHeight: `calc(100vh - ${props.theme.components.layout.composition.navbar.height})`,
						padding: "5em 2em",
						flexDirection: "column",
						justifyContent: "space-evenly",
					})}
				>
					<DataProviderContext.Consumer>
						{data =>
							data
								.getGalleryImages()
								.reduce<ImageModel[][]>((rows, image) => {
									const lastRow = rows.pop() || []
									return lastRow.length < 2 + ((rows.length + 1) % 2) ? [...rows, [...lastRow, image]] : [...rows, lastRow, [image]]
								}, [])
								.map((row, index) => (
									<FelaComponent
										key={index}
										style={{
											display: "flex",
											width: "100%",
											justifyContent: "space-evenly",
											flexWrap: "wrap",
										}}
									>
										{row.map((image, index) => (
											<FelaComponent
												key={index}
												style={(props: StyleProps<Theme>) => ({
													position: "relative",
													width: "300px",
													height: "200px",
													borderRadius: "10px",
													margin: "2rem",
													"::before": {
														content: '" "',
														position: "absolute",
														display: "block",
														width: "100%",
														height: "100%",
														top: "0",
														left: "0",
														transition: "opacity 0.5s",
														...props.theme.common.shadows.glow.normal.toFelaStyle(),
														opacity: "0.25",
													},
													":hover": {
														":before": {
															opacity: "0.5",
														},
													},
												})}
											>
												<FelaComponent
													style={{
														width: "100%",
														height: "100%",
													}}
												>
													{({ className }) => <Image className={className} image={image} />}
												</FelaComponent>
											</FelaComponent>
										))}
									</FelaComponent>
								))
						}
					</DataProviderContext.Consumer>
				</FelaComponent>
			</Layout>
		)
	}
}
