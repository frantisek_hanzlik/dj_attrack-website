import React from "react"
import NextDocument, { Head, Main, NextScript, DocumentContext } from "next/document"
import { renderToSheetList } from "fela-dom"
import { FelaRendererFactory } from "../../utils/fela/fela_renderer_factory"
import { PageBase } from "./client_page_base"

interface FelaSheet {
	type: string
	css: string
	media?: string
	support?: boolean
	rehydration: number
}

interface DocumentProps {
	felaSheetList: FelaSheet[]
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export class Document extends NextDocument<DocumentProps> {
	public static async getInitialProps(context: DocumentContext) {
		const felaRenderer = FelaRendererFactory.getNewFelaRenderer()
		const originalRenderPage = context.renderPage
		context.renderPage = () =>
			originalRenderPage({
				enhanceApp: App => props =>
					// eslint-disable-next-line @typescript-eslint/no-explicit-any
					(App as any) === PageBase ? (
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						React.createElement(App as any, {
							...props,
							felaRenderer,
						})
					) : (
						<App {...props} />
					),
			})
		return {
			...(await NextDocument.getInitialProps(context)),
			felaSheetList: renderToSheetList(felaRenderer),
		}
	}

	public render() {
		const styleNodes = this.props.felaSheetList.map(felaSheet => (
			<style
				dangerouslySetInnerHTML={{ __html: felaSheet.css }}
				data-fela-id=""
				data-fela-rehydration={felaSheet.rehydration}
				data-fela-support={felaSheet.support}
				data-fela-type={felaSheet.type}
				key={`${felaSheet.type}-${felaSheet.media}`}
				media={felaSheet.media}
			/>
		))

		return (
			<html>
				<Head>{styleNodes}</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</html>
		)
	}
}
