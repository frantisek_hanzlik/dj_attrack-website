import React from "react"
import { RendererProvider } from "react-fela"
import { FelaRendererFactory } from "../../utils/fela/fela_renderer_factory"
import { IRenderer } from "fela"
import { def } from "../../utils/default"

interface FelaProviderProps {
	renderer?: IRenderer
}

const fallbackRenderer = FelaRendererFactory.getNewFelaRenderer()

export class FelaProvider extends React.Component<FelaProviderProps> {
	public render() {
		const renderer = def(this.props.renderer, fallbackRenderer)
		return <RendererProvider renderer={renderer}>{this.props.children}</RendererProvider>
	}
}
