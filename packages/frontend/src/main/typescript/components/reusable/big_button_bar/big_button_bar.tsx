import React from "react"
import { Theme } from "../../../theming/theme"
import { connect, Rules, FelaWithStylesProps } from "react-fela"

interface BigButtonBarProps {
	children: React.ReactNode
}

interface Styles {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	bigButtonBar: any
}

export const BigButtonBar = connect((props => ({
	bigButtonBar: {
		display: "flex",
		position: "relative",
		justifyContent: "space-evenly",
		top: `calc(${props.theme.components.bigButton.width} / -2)`,
		marginBottom: `calc(${props.theme.components.bigButton.width} / -2)`,
	},
})) as Rules<BigButtonBarProps, Styles, Theme>)(
	class BigButtonBar extends React.Component<FelaWithStylesProps<BigButtonBarProps, Styles, Theme>> {
		public render() {
			return <div className={this.props.styles.bigButtonBar}>{this.props.children}</div>
		}
	},
)
