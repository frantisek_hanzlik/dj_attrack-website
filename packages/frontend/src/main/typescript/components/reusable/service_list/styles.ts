import { FelaStyle } from "../../../utils/fela/types"
import { Theme } from "../../../theming/theme"
import { flatMapObjectArray } from "../../../utils/array"
import { BoundingBox } from "../../../theming/properties/bounding_box"
import { ThemeStyles as ServiceButtonThemeStyles } from "./components/service_button_prototype"

export interface ThemeStyles {
	root: {
		bounds: BoundingBox[]
		background: {
			color: string
		}
	}
	subcomponents: {
		serviceButton: ServiceButtonThemeStyles
	}
}

export const styles: Record<"serviceList", FelaStyle<Theme>> = {
	serviceList: props => ({
		display: "flex",
		position: "relative",
		justifyContent: "space-evenly",
		alignItems: "center",
		flexWrap: "wrap",
		columnGap: "3rem",
		rowGap: "6rem",
		width: "100%",
		minHeight: "100vh",
		...props.theme.common.shadows.dark.inset.toFelaStyle(),
		...flatMapObjectArray(props.theme.components.serviceList.root.bounds, it => it.toFelaStyle()),
		backgroundColor: props.theme.components.serviceList.root.background.color,
	}),
}
